use std::path::Path;

pub fn get_parent(path: &Path) -> &Path {
    match path.parent() {
        Some(parent) => parent,
        None => Path::new("/"),
    }
    // todo!();
    // Path::new("something")
}
