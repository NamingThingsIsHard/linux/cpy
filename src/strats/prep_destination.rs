use std::error::Error;
use std::fs::create_dir_all;
use std::path::Path;

use crate::utils;

/// A strategy to make sure certain conditions are met before copying is attempted
pub trait PrepDestinationStratTrait {
    fn prep(&self, destination: &Path) -> Result<(), Box<dyn Error>>;
}

/// The simplest preparation is to make sure the parent directory actually exists before copying
pub struct CreateParentDirStrat {}

impl PrepDestinationStratTrait for CreateParentDirStrat {
    fn prep(&self, destination: &Path) -> Result<(), Box<dyn Error>> {
        create_dir_all(utils::get_parent(destination))?;
        Ok(())
    }
}
