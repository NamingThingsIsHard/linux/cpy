use std::error::Error;
use std::fmt;
use std::fmt::Formatter;

#[derive(Debug)]
pub struct BaseError {
    err: String,
}

impl BaseError {
    pub fn new(err: &str) -> BaseError {
        BaseError {
            err: err.to_string(),
        }
    }
}

impl fmt::Display for BaseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.err)
    }
}

impl Error for BaseError {}

macro_rules! throw {
    ($arg:tt) => {{
        return Err(Box::new(BaseError::new($arg)));
    }};
}

macro_rules! throw_fmt {
    ($($arg:tt)*) => {{
        return Err(Box::new(BaseError::new(format!($($arg)*).as_str())));
    }}
}
