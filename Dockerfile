FROM rust:slim-buster as base
ARG WORKDIR
WORKDIR ${WORKDIR:-/app}

ENV PATH="/root/.cargo/bin:$PATH"

# With full debug symbols
FROM base as dev
RUN rustup component add clippy \
    && rustup component add rustfmt

FROM base as prod
COPY . ./
RUN cargo build --release
