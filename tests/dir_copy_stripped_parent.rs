use std::fs::{create_dir_all, remove_dir_all, File};
use std::io;
use std::path::Path;

use kpy::copier::Copier;
use kpy::strats::build_destination::PrefixStrippedCountParentStrat;
use kpy::strats::copy::FileCopyStrat;
use kpy::strats::prep_destination::CreateParentDirStrat;

#[test]
fn copy_file() -> io::Result<()> {
    let copier = Copier {
        build_destination_strategy: Box::new(PrefixStrippedCountParentStrat { count: 2 }),
        copy_strategy: Box::new(FileCopyStrat {}),
        pre_destination_strategy: Box::new(CreateParentDirStrat {}),
    };
    // Create source file
    let source = Path::new("/dev/shm/a/b/c/file");
    create_dir_all(source.parent().expect("Get source parent")).expect("Touch source");
    File::create(source)?;

    // Prepare target dir
    let destination = Path::new("/dev/shm/target");
    if destination.exists() {
        remove_dir_all(destination).expect("Remove target");
    }

    // Actually copy the file
    let copy_result = copier.run(source, destination);
    assert!(
        copy_result.is_ok(),
        "Couldn't copy {:?}",
        copy_result.err().unwrap()
    );
    assert!(Path::new("/dev/shm/target/a/b/c/file").is_file());

    Ok(())
}

#[test]
fn copy_dir() -> io::Result<()> {
    let copier = Copier {
        build_destination_strategy: Box::new(PrefixStrippedCountParentStrat { count: 2 }),
        copy_strategy: Box::new(FileCopyStrat {}),
        pre_destination_strategy: Box::new(CreateParentDirStrat {}),
    };
    // Create source dir & file
    let dir = Path::new("/dev/shm/a/");
    let file = Path::new("/dev/shm/a/b/c/file");
    create_dir_all(file.parent().expect("Get source parent")).expect("Touch source");
    File::create(file)?;

    // Prepare target dir
    let destination = Path::new("/dev/shm/target");
    if destination.exists() {
        remove_dir_all(destination).expect("Remove target");
    }

    // Actually copy the dir
    let copy_result = copier.run(dir, destination);
    assert!(
        copy_result.is_ok(),
        "Couldn't copy {:?}",
        copy_result.err().unwrap()
    );
    assert!(Path::new("/dev/shm/target/a/b/c/file").is_file());

    Ok(())
}
